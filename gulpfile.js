/**
 * Created by Laf on 30/01/2016.
 */
'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var util = require('gulp-util');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var webserver = require('gulp-webserver');
//var imageminJpegtran = require('imagemin-jpegtran');

var defaultTasks = ['sass','copyFiles', 'imgs','browserify','scripts','webserver','watch'];
//'imgs',

gulp.task('webserver', function() {
    gulp.src('./dist')
        .pipe(webserver({
            livereload: false,
            directoryListing: false,
            open: true,
            fallback: 'test.html'
        }));
});

var scripts = ['./node_modules/jquery/dist/jquery.min.js','./node_modules/foundation-sites/dist/foundation.min.js','./src/js/bundle/test.js'];
var filesCopied = false;
var filesToCopy = {
    fontAwesome:{
        from:'./node_modules/font-awesome/fonts/**/*.*',
        to:'./dist/fonts'
    },
    customFonts:{
        from:'./src/fonts/**/*.*',
        to:'./dist/fonts'
    },
	foundation:{
		from:'./node_modules/foundation-sites/dist/foundation.css',
		to:'./dist/css'
	},
	index:{
		from:'./src/index.html',
		to:'./dist'
	},
	content:{
		from:'./src/content.json',
		to:'./dist'
	}
}

//minify jpgs
gulp.task('imgs', function() {
    return gulp.src('src/img/*.*')
        .pipe(imagemin({ progressive: true,svgoPlugins: [{removeViewBox: false}]}))
        .pipe(gulp.dest('dist/img'));
});


//Concatenate Scripts
gulp.task('scripts', function() {
    return gulp.src(scripts)
        .pipe(concat('test.js'))
        .pipe(gulp.dest('./dist/js/'));
});

//Browserify
gulp.task('browserify',function(){
    return browserify('./src/js/index.js')
        .bundle()
        .pipe(source('test.js'))
        .pipe(gulp.dest('./src/js/bundle/'));
});


//copy Files
gulp.task('copyFiles',function(){
    if(filesCopied === false){
        var k, target;
        for(k in filesToCopy){
            if(filesToCopy.hasOwnProperty(k)){
                target = filesToCopy[k];
                gulp.src(target.from).pipe(gulp.dest(target.to));
                util.log(k + ' files copied from: ' + target.from + ' to: ' + target.to);
            }
        }
        filesCopied = true;
    }else{
        util.log('files already copied -> rerun Gulp to copy again');
    }
});




//Sass
gulp.task('sass',function(){
    gulp.src('./src/scss/test.scss')
        .pipe(sass({outputStyle:'compressed'}))
        .pipe(gulp.dest('./dist/css'));
});

//Watch
gulp.task('watch',function(){
    gulp.watch('./src/scss/**/*.scss',['sass']);
    gulp.watch('./src/js/**/*.js',['browserify','scripts']);

});

//default
gulp.task('default',defaultTasks);
