/**
 * Created by Laf on 5/02/2016.
 */
'use strict';
var ConstructElements = function(){
    var that = this;
    this.thumbnail = function(src,alt){
        var theImg = document.createElement('img');
        theImg.src = src;
        theImg.alt = (alt != '' || alt !=null) ? alt : 'image alt tag';
        return theImg;
    };
    this.paragraph = function(text){
        var theParagraph = document.createElement('p');
        theParagraph.innerHTML = text;
        return theParagraph;
    }
    this.next = function(text){
        var next = document.createElement('a');
        next.innerText = text;
        next.id = 'nextText';
        var img = that.thumbnail('img/ARROWRight.png','next');
        next.appendChild(img);
        return next;
    }
    this.prev = function(text){
        var p = document.createElement('a');
        p.id = 'prevText';
        p.innerHTML = '<img src="img/arrowLeft.png"/><span >' + text + '</span>';
        return p;
    }

};

module.exports = ConstructElements;