/**
 * Created by Laf on 5/02/2016.
 */

var LoadJson = function(){

    this.loadJSON = function(path,callback) {
        var jsonObj = new XMLHttpRequest();
        jsonObj.overrideMimeType("application/json");
        jsonObj.open('GET', path, true); // Replace 'my_data' with the path to your file
        jsonObj.onreadystatechange = function () {
            if (jsonObj.readyState == 4 && jsonObj.status == "200") {
                callback(jsonObj.responseText);
            }
        };
        jsonObj.send(null);
    }
};

module.exports = LoadJson;
