/**
 * Created by Laf on 30/01/2016.
 */
'use strict';
$(document).foundation();
var loadJson = require('./modules/loadJson.js');
var construct = require('./modules/constructElements.js');
var lj = new loadJson();
var ce = new construct();
var theData;

lj.loadJSON('./content.json',function(response){
    var k, current = 0,currentContent= 0, theData = JSON.parse(response);
    var expanded = 1, content = theData.content, contentLength = content.length - 1;
    var theObj = {}, thumb = 'roger.jpeg'; // fallback
    var appended = 0;
    var imgSelector = document.getElementById('productImage');
    var descriptionSelector = document.getElementById('productText');
    var nextSelector = document.getElementById('next');
    var prevSelector = document.getElementById('prev');

    function loader(){
        document.getElementById('titleArrow').addEventListener('click',function(){
            if(expanded === 1){
                this.style.transform = "rotate(180deg)";
                document.getElementById('contentContainer').style.maxHeight = 0;
                expanded = 0;
            }else{
                this.style.transform = "rotate(0deg)";
                document.getElementById('contentContainer').style.maxHeight = '1000px';
                expanded = 1;
            }
        });
    };

    function addListeners(callback) {
        document.getElementById('prevText').addEventListener('click', prevHandler);
        document.getElementById('nextText').addEventListener('click', nextHandler);
        if(typeof(callback)=='function'){
            callback();
        }
    }

    function contentChange(callback) {
        if (appended === 0) {
            imgSelector.style.opacity = 0;
            descriptionSelector.style.opacity = 0;
            nextSelector.style.opacity = 0;
            prevSelector.style.opacity = 0;
            setTimeout(function(){
            imgSelector.appendChild(theObj[0].thumb);
            descriptionSelector.appendChild(theObj[0].description);
            nextSelector.appendChild(theObj[0].next);
            prevSelector.appendChild(theObj[0].prev);
            appended = 1;
            if(typeof(callback) == 'function'){
                callback();
            }
                document.getElementById('ajaxLoader').style.display = 'none';
            imgSelector.style.opacity = 1;
            descriptionSelector.style.opacity = 1;
            nextSelector.style.opacity = 1;
            prevSelector.style.opacity = 1;
            },500)
        } else {
            imgSelector.style.opacity = 0;
            descriptionSelector.style.opacity = 0;
            nextSelector.style.opacity = 0;
            prevSelector.style.opacity = 0;
            setTimeout(function(){
                var prevTxt = document.getElementById('prevText');
                var nextTxt = document.getElementById('nextText');
                prevTxt.removeEventListener('click',prevHandler);
                nextTxt.removeEventListener('click',nextHandler);
                var prodImg = document.getElementById('productImage').getElementsByTagName('img')[0];
                var prodTxt = document.getElementById('productText').getElementsByTagName('p')[0];
                prodImg.parentNode.removeChild(prodImg);
                prodTxt.parentNode.removeChild(prodTxt);
                nextTxt.parentNode.removeChild(nextTxt);
                prevTxt.parentNode.removeChild(prevTxt);
                imgSelector.appendChild(theObj[currentContent].thumb);
                descriptionSelector.appendChild(theObj[currentContent].description);
                nextSelector.appendChild(theObj[currentContent].next);
                prevSelector.appendChild(theObj[currentContent].prev);
                if(typeof(callback) == 'function'){
                    callback();
                }
                imgSelector.style.opacity = 1;
                descriptionSelector.style.opacity = 1;
                nextSelector.style.opacity = 1;
                prevSelector.style.opacity = 1;
            },500)
        }
    }

    function nextHandler (){
        if (currentContent === contentLength) {
            currentContent = 0;
        } else {
            currentContent += 1;
        };
        contentChange(function(){
            addListeners();
        });
    }

    function prevHandler (){
        if (currentContent === 0) {
            currentContent = contentLength
        } else {
            currentContent -= 1;
        };
        contentChange(function(){
            addListeners();
        });
    }
    document.getElementById('widgetTitle').innerHTML = theData.title;

    for(k in content){
        if(content.hasOwnProperty(k)){
            theObj[current] = {};
            theObj[current].title = content[k].title;
            theObj[current].description = ce.paragraph(content[k].description);
            thumb = content[k].thumbnail !== '' && content[k].thumbnail !== undefined ? content[k].thumbnail : thumb;
            theObj[current].thumb = ce.thumbnail('img/' + thumb,content[k].title);
            if(current === 0){
                theObj[current].prev = false;
            }else{
                theObj[current].prev = ce.prev(theObj[current - 1].title);
            }if(current === contentLength){
                theObj[current].next = false;
            }
        }
        current ++;
    }
    for(var i = contentLength;i>=0;i--){
        if(i === contentLength){
            theObj[contentLength].next = ce.next(theObj[0].title);
            theObj[i - 1].next = ce.next(theObj[i].title);
        }else if(i === 0){
            theObj[0].prev = ce.prev(theObj[contentLength].title);
        }else {
            theObj[i - 1].next = ce.next(theObj[i].title);
        }
    }








    contentChange(function(){
        addListeners(loader());
    });





})