# Gumtree Code Test

**requirement**

* Node
* Gulp (installed Globally)
* **may** require ruby to be installed (not tested)

**Installation**

in the command line, run: 

npm i

followed by:

gulp

**Troubleshooting**

if the webserver starts and you get a message "cannot /Get" -> ctrl+C out of Gulp and run Gulp again

Images missing -> ctrl+c out of Gulp and run Gulp again

Ajax Loader won't disappear -> ctrl+c out of Gulp and run Gulp again



anything else:

contact mr.lafraldo@gmail.com